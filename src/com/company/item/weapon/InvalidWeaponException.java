package com.company.item.weapon;

public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
