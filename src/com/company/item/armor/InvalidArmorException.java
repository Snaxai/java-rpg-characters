package com.company.item.armor;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
