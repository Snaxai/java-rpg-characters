package com.company.character;

import com.company.item.armor.Armor;
import com.company.item.armor.InvalidArmorException;
import com.company.item.weapon.InvalidWeaponException;
import com.company.item.weapon.Weapon;

public interface RPG_Character {
    void levelUp();

    void levelUp(int intelligence, int dexterity, int strength);

    boolean equip(Armor armor) throws InvalidArmorException;

    boolean equip(Weapon weapon) throws InvalidWeaponException;

    double characterDps();
}
