package com.company;

import com.company.attributes.PrimaryAttributes;
import com.company.character.*;
import com.company.item.armor.Armor;
import com.company.item.armor.ArmorType;
import com.company.item.Slot;
import com.company.item.armor.InvalidArmorException;
import com.company.item.weapon.InvalidWeaponException;
import com.company.item.weapon.Weapon;
import com.company.item.weapon.WeaponType;

public class Main {

    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException {
        Armor chest = new
                Armor("worn out robe",
                1,
                Slot.BODY,
                ArmorType.CLOTH,
                new PrimaryAttributes(1, 2, 3));
        Armor chest1 = new
                Armor("worn out robe",
                1,
                Slot.BODY,
                ArmorType.PLATE,
                new PrimaryAttributes(5, 2, 3));
        Armor legs = new
                Armor("worn out robe",
                1,
                Slot.LEGS,
                ArmorType.CLOTH,
                new PrimaryAttributes(3, 4, 5));
        Weapon testStaff = new Weapon("Old staff", 1, WeaponType.STAFF,
                15, 1.6);
        Weapon testAxe = new Weapon("Old axe", 1, WeaponType.AXE,
                10, 1.6);
        Mage myMage = new Mage("SnaxaiMage");
        Warrior myWarr = new Warrior("SnaxaiWarr");
        myWarr.equip(testAxe);
        myWarr.equip(chest1);
        myMage.equip(testStaff);
        myMage.equip(legs);
        myMage.equip(chest);
        myMage.levelUp();
        myMage.levelUp();
        myWarr.levelUp();
        myWarr.printInfo();
        myMage.printInfo();
    }
}
