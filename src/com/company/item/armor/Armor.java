package com.company.item.armor;

import com.company.attributes.PrimaryAttributes;
import com.company.item.Item;
import com.company.item.Slot;

public class Armor extends Item {
    private ArmorType armorType;
    private PrimaryAttributes attributes;

    // Constructor
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, PrimaryAttributes attributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    // Setters and getters
    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttributes attributes) {
        this.attributes = attributes;
    }
}