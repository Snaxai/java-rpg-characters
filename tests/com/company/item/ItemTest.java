package com.company.item;

import com.company.attributes.PrimaryAttributes;
import com.company.character.Warrior;
import com.company.item.armor.Armor;
import com.company.item.armor.ArmorType;
import com.company.item.armor.InvalidArmorException;
import com.company.item.weapon.InvalidWeaponException;
import com.company.item.weapon.Weapon;
import com.company.item.weapon.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void equip_WeaponAxeRequiredLevel1_ShouldBeValidEquipment() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testWeapon = new Weapon(
                "Common Axe", 1, WeaponType.AXE,
                7, 1.1);

        // Act
        // Assert
    }

    @Test
    void equip_WeaponAxeWithRequiredLevel2_ShouldThrowInvalidWeaponException() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testWeapon = new Weapon(
                "Common Axe", 2, WeaponType.AXE,
                7, 1.1);
        String expected = "You can not equip this weapon. Too high level requirement";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> myWarr.equip(testWeapon));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equip_ArmorWithRequiredLevel2_ShouldThrowInvalidArmorException() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Armor testPlateBody = new Armor("Common Plate Body Armor",
                2, Slot.BODY, ArmorType.PLATE,
                new PrimaryAttributes(1, 0, 0));
        String expected = "You cannot equip this armor. Too high level requirement";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> myWarr.equip(testPlateBody));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equip_WarriorTryToEquipWrongWeaponType_ShouldThrowInvalidWeaponException() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testBow = new Weapon(
                "Common Bow", 1, WeaponType.BOW,
                12, 0.8);
        String expected = "You cannot equip this type of weapon";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> myWarr.equip(testBow));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equip_aWarriorTryToEquipWrongArmorType_ShouldThrowInvalidArmorException() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Armor testClothHead = new Armor("Common Cloth Head Armor",
                1, Slot.HEAD, ArmorType.CLOTH,
                new PrimaryAttributes(0, 0, 5));
        String expected = "You cannot equip this type of armor";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> myWarr.equip(testClothHead));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equip_ValidWeapon_ShouldReturnTrue() throws InvalidWeaponException {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testWeapon = new Weapon(
                "Common Axe", 1, WeaponType.AXE,
                7, 1.1);
        boolean expected = true;
        // Act
        boolean actual = myWarr.equip(testWeapon);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void equip_ValidArmor_ShouldReturnTrue() throws InvalidArmorException {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Armor testPlateBody = new Armor("Common Plate Body Armor",
                1, Slot.BODY, ArmorType.PLATE,
                new PrimaryAttributes(1, 0, 0));
        boolean expected = true;
        // Act
        boolean actual = myWarr.equip(testPlateBody);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void characterDps_noWeaponEquipped_ShouldCalculateWithDefault1() {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        double expected = 1 * (1 + (5 / 100));
        // Act
        double actual = myWarr.characterDps();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void characterDps_validWeaponEquipped_ShouldCalculateWithWeaponDps() throws InvalidWeaponException {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testWeapon = new Weapon(
                "Common Axe", 1, WeaponType.AXE,
                7, 1.1);
        myWarr.equip(testWeapon);
        double expected = (7 * 1.1) * (1 + (5 / 100));
        // Act
        double actual = myWarr.characterDps();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void characterDps_validWeaponAndArmorEquipped_ShouldCalculateWithWeaponDpsAndTotalAttributes() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        Warrior myWarr = new Warrior("myWarrior");
        Weapon testWeapon = new Weapon(
                "Common Axe", 1, WeaponType.AXE,
                7, 1.1);
        myWarr.equip(testWeapon);
        Armor testPlateBody = new Armor("Common Plate Body Armor",
                1, Slot.BODY, ArmorType.PLATE,
                new PrimaryAttributes(1, 0, 0));
        myWarr.equip(testPlateBody);
        double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
        // Act
        double actual = myWarr.characterDps();
        // Assert
        assertEquals(expected, actual);
    }
}