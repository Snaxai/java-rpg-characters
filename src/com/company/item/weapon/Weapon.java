package com.company.item.weapon;

import com.company.item.Item;
import com.company.item.Slot;

public class Weapon extends Item {
    private WeaponType weaponType;
    private int damage;
    private double attackSpeed;

    public Weapon(String name, int requiredLevel, WeaponType type, int damage, double attackSpeed) {
        super(name, requiredLevel, Slot.WEAPON);
        this.weaponType = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double getWeaponDps() {
        return damage * attackSpeed;
    }
}
