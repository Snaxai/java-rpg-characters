package com.company.character;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void character_createNewCharacter_ShouldBeLevel1() {
        Mage myMage1 = new Mage("myMage1");
        int expected = 1;

        int actual = myMage1.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_levelUpACharacter_ShouldIncreaseItsLevelBy1() {
        Mage testMage = new Mage("testMage");
        int expected = 2;

        testMage.levelUp();
        int actual = testMage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void Character_newMage_ShouldHaveProperDefaultAttributes() {
        Mage myMage = new Mage("myMage");
        int[] expected = {1, 1, 8};

        int[] actual = {myMage.getBaseAttributes().getStrength(),
                myMage.getBaseAttributes().getDexterity(),
                myMage.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void Warrior_newWarrior_ShouldHaveProperDefaultAttributes() {
        Warrior myWarrior = new Warrior("myWarrior");
        int[] expected = {5, 2, 1};

        int[] actual = {myWarrior.getBaseAttributes().getStrength(),
                myWarrior.getBaseAttributes().getDexterity(),
                myWarrior.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void Rogue_newRogue_ShouldHaveProperDefaultAttributes() {
        Rogue myRogue = new Rogue("myRogue");
        int[] expected = {2, 6, 1};

        int[] actual = {myRogue.getBaseAttributes().getStrength(),
                myRogue.getBaseAttributes().getDexterity(),
                myRogue.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void Ranger_newRanger_ShouldHaveProperDefaultAttributes() {
        Ranger myRanger = new Ranger("myRanger");
        int[] expected = {1, 7, 1};

        int[] actual = {myRanger.getBaseAttributes().getStrength(),
                myRanger.getBaseAttributes().getDexterity(),
                myRanger.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_levelUpRanger_ShouldHaveAttributesIncreaseCorrect() {
        Ranger myRanger = new Ranger("myRanger");
        myRanger.levelUp();
        int[] expected = {1 + 1, 7 + 5, 1 + 1};

        int[] actual = {myRanger.getBaseAttributes().getStrength(),
                myRanger.getBaseAttributes().getDexterity(),
                myRanger.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_levelUpMage_ShouldHaveAttributesIncreaseCorrect() {
        Mage myMage = new Mage("myMage");
        myMage.levelUp();
        int[] expected = {1 + 1, 1 + 1, 8 + 5};

        int[] actual = {myMage.getBaseAttributes().getStrength(),
                myMage.getBaseAttributes().getDexterity(),
                myMage.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_levelUpWarrior_ShouldHaveAttributesIncreaseCorrect() {
        Warrior myWarrior = new Warrior("myWarrior");
        myWarrior.levelUp();
        int[] expected = {5 + 3, 2 + 2, 1 + 1};

        int[] actual = {myWarrior.getBaseAttributes().getStrength(),
                myWarrior.getBaseAttributes().getDexterity(),
                myWarrior.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }

    @Test
    void levelUp_levelUpRogue_ShouldHaveAttributesIncreaseCorrect() {
        Rogue myRogue = new Rogue("myRogue");
        myRogue.levelUp();
        int[] expected = {2 + 1, 6 + 4, 1 + 1};

        int[] actual = {myRogue.getBaseAttributes().getStrength(),
                myRogue.getBaseAttributes().getDexterity(),
                myRogue.getBaseAttributes().getIntelligence()
        };
        assertArrayEquals(expected, actual);
    }
}