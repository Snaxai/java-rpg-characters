package com.company.character;

import com.company.attributes.PrimaryAttributes;
import com.company.item.armor.ArmorType;
import com.company.item.weapon.WeaponType;

import java.util.HashMap;

public class Warrior extends Character {
    private static final PrimaryAttributes baseAttributes =
            new PrimaryAttributes(5, 2, 1);

    private static final int strenghtPerLevel = 3;
    private static final int dexterityPerLevel = 2;
    private static final int intelligencePerLevel = 1;

    private static final ArmorType[] eligibleArmorTypes = {ArmorType.PLATE};
    private static final WeaponType[] eligibleWeaponTypes = {WeaponType.AXE};

    public Warrior(String name) {
        super(name, baseAttributes, eligibleArmorTypes, eligibleWeaponTypes);
    }

    // level up method with attributes specific for warrior
    @Override
    public void levelUp() {
        super.levelUp(strenghtPerLevel, dexterityPerLevel, intelligencePerLevel);
    }

    // Calculates character dps
    @Override
    public double characterDps() {
        calcEquipmentAttributes();
        double characterDps = getWeaponDps() * (1 + getTotalStrength() / 100d);
        return characterDps;
    }
}
