package com.company.character;

import com.company.attributes.PrimaryAttributes;
import com.company.item.armor.Armor;
import com.company.item.Item;
import com.company.item.Slot;
import com.company.item.armor.ArmorType;
import com.company.item.armor.InvalidArmorException;
import com.company.item.weapon.InvalidWeaponException;
import com.company.item.weapon.Weapon;
import com.company.item.weapon.WeaponType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public abstract class Character implements RPG_Character {
    protected String name;
    protected int level = 1;
    protected PrimaryAttributes baseAttributes;
    protected PrimaryAttributes attributesFromEquipment =
            new PrimaryAttributes(0, 0, 0);
    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected ArmorType[] eligibleArmorTypes;
    protected WeaponType[] eligibleWeaponTypes;

    // Constructor
    public Character(String name, PrimaryAttributes baseAttributes, ArmorType[] eligibleArmorTypes, WeaponType[] eligibleWeaponTypes) {
        this.name = name;
        this.baseAttributes = baseAttributes;
        this.eligibleArmorTypes = eligibleArmorTypes;
        this.eligibleWeaponTypes = eligibleWeaponTypes;
    }

    // Prints character info
    public void printInfo() {
        calcEquipmentAttributes();
        System.out.println("---------------------");
        System.out.println("NAME:         " + this.name);
        System.out.println("LEVEL:        " + this.level);
        System.out.println("Strength:     " + getTotalStrength());
        System.out.println("Dexterity:    " + getTotalDexterity());
        System.out.println("Intelligence: " + getTotalIntelligence());
        System.out.println("DPS:          " + characterDps());
    }

    // level up. increments level and adds attributes
    @Override
    public void levelUp(int strength, int dexterity, int intelligence) {
        this.level++;
        int newStrength = this.baseAttributes.getStrength() + strength;
        int newDexterity = this.baseAttributes.getDexterity() + dexterity;
        int newIntelligence = this.baseAttributes.getIntelligence() + intelligence;
        this.baseAttributes = new PrimaryAttributes(newStrength, newDexterity, newIntelligence);
    }

    // get the weapon damage. return 1 if no weapon
    public int getWeaponDamage() {
        if (!this.equipment.containsKey(Slot.WEAPON)) return 1;
        else {
            Item weapon = equipment.get(Slot.WEAPON);
            return ((Weapon) weapon).getDamage();
        }
    }

    // get the weapon dps. return 1 if no weapon
    public double getWeaponDps() {
        if (!this.equipment.containsKey(Slot.WEAPON)) return 1;
        else {
            Weapon weapon = (Weapon) equipment.get(Slot.WEAPON);
            return weapon.getWeaponDps();
        }
    }

    // Equip armor
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {

        boolean canEquip = Arrays.stream(this.eligibleArmorTypes).anyMatch(Predicate.isEqual(armor.getArmorType()));
        if (!canEquip) {
            throw new InvalidArmorException("You cannot equip this type of armor");
        }
        if (armor.getRequiredLevel() > this.level) {
            throw new InvalidArmorException("You cannot equip this armor. Too high level requirement");
        }
        equipment.put(armor.getSlot(), armor);
        return true;
    }

    // Equip weapon
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {

        boolean canEquip = Arrays.stream(this.eligibleWeaponTypes).anyMatch(Predicate.isEqual(weapon.getWeaponType()));
        if (!canEquip) {
            throw new InvalidWeaponException("You cannot equip this type of weapon");
        }
        if (weapon.getRequiredLevel() > this.level) {
            throw new InvalidWeaponException("You can not equip this weapon. Too high level requirement");
        }
        equipment.put(weapon.getSlot(), weapon);
        return true;
    }

    // Calcs the attributes from equipment
    public void calcEquipmentAttributes() {
        for (Map.Entry<Slot, Item> item : equipment.entrySet()) {
            if (item.getValue().getSlot() != Slot.WEAPON) {
                this.attributesFromEquipment.addDexterity(((Armor) item.getValue()).getAttributes().getDexterity());
                this.attributesFromEquipment.addIntelligence(((Armor) item.getValue()).getAttributes().getIntelligence());
                this.attributesFromEquipment.addStrength(((Armor) item.getValue()).getAttributes().getStrength());
            }
        }
    }

    // Setters and getters
    public int getTotalStrength() {
        return baseAttributes.getStrength() + this.attributesFromEquipment.getStrength();
    }

    public int getTotalDexterity() {
        return baseAttributes.getDexterity() + this.attributesFromEquipment.getDexterity();
    }

    public int getTotalIntelligence() {
        return baseAttributes.getIntelligence() + this.attributesFromEquipment.getIntelligence();
    }

    public PrimaryAttributes getAttributesFromEquipment() {
        return attributesFromEquipment;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }
}
