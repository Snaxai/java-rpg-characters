package com.company.attributes;

public class PrimaryAttributes {
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void addStrength(int strength) {
        this.strength += strength;
    }

    public void addDexterity(int dexterity) {
        this.dexterity += dexterity;
    }

    public void addIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }
}
