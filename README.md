## Java rpg characters
This is an assignment for the Java part of the java fullstack course at Noroff in the Experis Academy program.

This assignment is a console application of RPG characters.
Various character classes having attributes which increase at different rates as the character gains levels.
Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of
the character, causing it to deal more damage. Certain characters can equip certain item types.

### contains: 
 - Custom exceptions
 - Full test coverage of the functionality

## Author
Daniel Mossestad
