package com.company.character;

import com.company.attributes.PrimaryAttributes;
import com.company.item.armor.ArmorType;
import com.company.item.weapon.WeaponType;

public class Ranger extends Character {
    private static final PrimaryAttributes baseAttributes =
            new PrimaryAttributes(1, 7, 1);

    private static final int strenghtPerLevel = 1;
    private static final int dexterityPerLevel = 5;
    private static final int intelligencePerLevel = 1;

    private static final ArmorType[] eligibleArmorTypes = {ArmorType.LEATHER, ArmorType.MAIL};
    private static final WeaponType[] eligibleWeaponTypes = {WeaponType.AXE};

    public Ranger(String name) {
        super(name, baseAttributes, eligibleArmorTypes, eligibleWeaponTypes);
    }

    // level up method with attributes specific for ranger
    @Override
    public void levelUp() {
        super.levelUp(strenghtPerLevel, dexterityPerLevel, intelligencePerLevel);
    }

    // Calculates character dps
    @Override
    public double characterDps() {
        calcEquipmentAttributes();
        double characterDps = getWeaponDps() * (1 + getTotalDexterity() / 100d);
        return characterDps;
    }
}
