package com.company.item.armor;

public enum ArmorType {
    PLATE,
    CLOTH,
    LEATHER,
    MAIL
}
