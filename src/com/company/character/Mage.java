package com.company.character;

import com.company.attributes.PrimaryAttributes;
import com.company.item.armor.ArmorType;
import com.company.item.weapon.WeaponType;

public class Mage extends Character {
    private static final PrimaryAttributes baseAttributes =
            new PrimaryAttributes(1, 1, 8);

    private static final int strenghtPerLevel = 1;
    private static final int dexterityPerLevel = 1;
    private static final int intelligencePerLevel = 5;

    private static final ArmorType[] eligibleArmorTypes = {ArmorType.CLOTH};
    private static final WeaponType[] eligibleWeaponTypes = {WeaponType.STAFF};

    // Constructor. Setting name and attributes
    public Mage(String name) {
        super(name, baseAttributes, eligibleArmorTypes, eligibleWeaponTypes);
    }

    // level up method with attributes specific for mage
    @Override
    public void levelUp() {
        super.levelUp(strenghtPerLevel, dexterityPerLevel, intelligencePerLevel);
    }

    // Calculates character dps
    @Override
    public double characterDps() {
        double characterDps = getWeaponDps() * (1 + (getTotalIntelligence() / 100d));
        return characterDps;
    }
}
